import numpy as np
import matplotlib.pyplot as plt
import xlsxwriter
import pandas as pd
import sys
import scipy
from scipy import optimize, signal
from lmfit.models import LinearModel, LorentzianModel #imoports linear and lorentzian model fitting

df = pd.read_excel('Final_analysis.xlsx', sheetname='Fe3O4Calc', index_col=None, na_values=['NA'],parse_cols= [5,6,8]) #parses out columns that we want

x = df['channel']
y = df['value']
correctedy=(df['value']-df['quad']) #removing quadratic effect

xoffset= 5350 #data skip offset to 5350 on xvalues
ysmooth=scipy.signal.savgol_filter(correctedy,155,3) #smooth curve

peaks = scipy.signal.find_peaks(-ysmooth,height=20, distance=300)#finds peaks in ysmooth (inverted y values)

peak_pos = peaks[0] #finding peaks
peak_pos_shifted= [peak + xoffset for peak in peak_pos] #offset peaks
peak_vals = peaks[1]["peak_heights"]
peak_vals = -peak_vals
print(peak_pos_shifted)

plt.plot(df['channel'],correctedy)
plt.ylabel('Value')
plt.xlabel('Channel') 
plt.xlim((6000,11000)) #trimmed the edges of the data
plt.plot(df['channel'],ysmooth,'r') #make the curve red
plt.plot(peak_pos_shifted, peak_vals, 'go')
plt.show()

lorentz_model= LorentzianModel()
pars = lorentz_model.guess(ysmooth, x=x)

lorentz1 = LorentzianModel(prefix= 'peak1_')
pars.update(lorentz1.make_params())
pars['peak1_center'].set(6893, min=6890, max=6900)

lorentz2 = LorentzianModel(prefix= 'peak2_')
pars.update(lorentz2.make_params())
pars['peak2_center'].set(7539, min=7530, max=7545)

lorentz3 = LorentzianModel(prefix= 'peak3_')
pars.update(lorentz3.make_params())
pars['peak3_center'].set(8169, min=8165, max=8175)

lorentz4 = LorentzianModel(prefix= 'peak4_')
pars.update(lorentz4.make_params())
pars['peak4_center'].set(8642, min=8635, max=8650)

lorentz5 = LorentzianModel(prefix= 'peak5_')
pars.update(lorentz5.make_params())
pars['peak5_center'].set(9267, min=9260, max=9270)

lorentz6 = LorentzianModel(prefix= 'peak6_')
pars.update(lorentz6.make_params())
pars['peak6_center'].set(9907, min=9900, max=9910)


mod= lorentz1 + lorentz2 + lorentz3 + lorentz4 + lorentz5 + lorentz6 #6 lorentzians, one for each of 6 peaks, modeled together
init = mod.eval(pars, x=x)
out = mod.fit(ysmooth, pars, x=x)
print(out.fit_report(min_correl=0.5))
plt.plot(df['channel'], ysmooth, 'b')
plt.plot(df['channel'], init, 'r-')
plt.plot(df['channel'], out.best_fit, 'r-')
plt.show()
