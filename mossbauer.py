import numpy as np
import matplotlib.pyplot as plt
#import scipy
#import scipy.interpolate
#import scipy.fftpack
#import glob
import xlsxwriter
import pandas as pd
import sys
import scipy
from scipy import signal

df = pd.read_excel('Final_analysis.xlsx', sheetname='IronFoilCalc', index_col=None, na_values=['NA'],parse_cols= [5,6])

df2 = pd.read_excel('Final_analysis.xlsx', sheetname='IronFoilCalc', index_col=None, na_values=['NA'],parse_cols= [8]) 

x = df['channel']
y = df['value']

correctedy=(df['value']-df2['quad'])

ysmooth=scipy.signal.savgol_filter(correctedy,155,3)

def lorentzian (x,*p):
	return p[0]+(p[1]/np.pi)/(1.0+((x-p[2])/p[3])**2)

plt.plot(df['channel'],correctedy)
plt.ylabel('Value')
plt.xlabel('Channel')
plt.xlim((5442,11285))
plt.plot(df['channel'],ysmooth,'r') 
plt.show()



