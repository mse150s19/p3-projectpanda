import numpy as np
import matplotlib.pyplot as plt
#import scipy
#import scipy.interpolate
#import scipy.fftpack
#import glob
import xlsxwriter
import pandas as pd
import sys
import scipy
from scipy import optimize, signal
from lmfit.models import LinearModel, LorentzianModel #imports linear and lorentzian fitting

df = pd.read_excel('Final_analysis.xlsx', sheetname='IronFoilCalc', index_col=None, na_values=['NA'],parse_cols= [5,6,8]) #parses out columns that we want


x = df['channel']
y = df['value']

correctedy=(df['value']-df['quad']) #removing quadratic effect

xoffset= 5280 #creates xvalue offset of 5280
ysmooth=scipy.signal.savgol_filter(correctedy,155,3) #smooth curve

peaks = scipy.signal.find_peaks(-ysmooth,height=100, distance=75)#finds peaks in ysmooth (inverted y values)

peak_pos = peaks[0] #finding peaks
peak_pos_shifted= [peak + xoffset for peak in peak_pos] #offset peaks
peak_vals = peaks[1]["peak_heights"]
peak_vals = -peak_vals #makes peak values negative so it finds minimums instead of maximums
print(peak_pos_shifted)

plt.plot(df['channel'],correctedy)
plt.ylabel('Value')
plt.xlabel('Channel') 
plt.xlim((5500,11285)) #trimmed the edges of the data
plt.plot(df['channel'],ysmooth,'r') #make the curve red
plt.plot(peak_pos_shifted, peak_vals, 'go') #plots shifted peak values
plt.show()

lorentz_model= LorentzianModel()
pars = lorentz_model.guess(ysmooth, x=x)

lorentz1 = LorentzianModel(prefix= 'peak1_')
pars.update(lorentz1.make_params())
pars['peak1_center'].set(7324, min=7200, max=7500)

lorentz2 = LorentzianModel(prefix= 'peak2_')
pars.update(lorentz2.make_params())
pars['peak2_center'].set(7754, min=7500, max=8000)

lorentz3 = LorentzianModel(prefix= 'peak3_')
pars.update(lorentz3.make_params())
pars['peak3_center'].set(8175, min=8000, max=8250)

lorentz4 = LorentzianModel(prefix= 'peak4_')
pars.update(lorentz4.make_params())
pars['peak4_center'].set(8495, min=8250, max=8500)

lorentz5 = LorentzianModel(prefix= 'peak5_')
pars.update(lorentz5.make_params())
pars['peak5_center'].set(8917, min=8900, max=9000)

lorentz6 = LorentzianModel(prefix= 'peak6_')
pars.update(lorentz6.make_params())
pars['peak6_center'].set(9350, min=9200, max=9500)


mod= lorentz1 + lorentz2 + lorentz3 + lorentz4 + lorentz5 + lorentz6
init = mod.eval(pars, x=x)
out = mod.fit(ysmooth, pars, x=x)
print(out.fit_report(min_correl=0.5))
plt.plot(df['channel'], ysmooth, 'b')
plt.plot(df['channel'], init, 'r-')
plt.plot(df['channel'], out.best_fit, 'r-')
plt.show()
