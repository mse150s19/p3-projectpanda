import numpy as np
import matplotlib.pyplot as plt
import xlsxwriter
import pandas as pd
import sys
import scipy
from scipy import optimize, signal
from lmfit.models import LinearModel, LorentzianModel #imports lorentzian and linear fitting

df = pd.read_excel('Final_analysis.xlsx', sheetname='Fe2O3Calc', index_col=None, na_values=['NA'],parse_cols= [5,6,8]) #parses out columns that we want

x = df['channel']
y = df['value']
correctedy=(df['value']-df['quad']) #removing quadratic effect

xoffset= 5300 #selects xvalue offset of 5300
ysmooth=scipy.signal.savgol_filter(correctedy,101,3) #smooths curve

peaks = scipy.signal.find_peaks(-ysmooth,height=40, distance=380)#finds peaks in ysmooth (inverted y values)

peak_pos = peaks[0]
peak_pos_shifted= [peak + xoffset for peak in peak_pos]
peak_vals = peaks[1]["peak_heights"]
peak_vals = -peak_vals
print(peak_pos_shifted)

plt.plot(df['channel'],correctedy)
plt.ylabel('Value')
plt.xlabel('Channel')
plt.xlim((6000, 11000))
plt.plot(df['channel'],ysmooth,'r') #make the curve red
plt.plot(peak_pos_shifted, peak_vals, 'go')
plt.show()

lorentz_model= LorentzianModel()
pars = lorentz_model.guess(ysmooth, x=x)
pars_init = lorentz_model.guess(ysmooth, x=x)

lorentz1 = LorentzianModel(prefix= 'peak1_')
pars.update(lorentz1.make_params())
pars['peak1_center'].set(6848, min=6800, max=6900)

lorentz2 = LorentzianModel(prefix= 'peak2_')
pars.update(lorentz2.make_params())
pars['peak2_center'].set(7528, min=7500, max=7580)

lorentz3 = LorentzianModel(prefix= 'peak3_')
pars.update(lorentz3.make_params())
pars['peak3_center'].set(8186, min=8120, max=8200)

lorentz4 = LorentzianModel(prefix= 'peak4_')
pars.update(lorentz4.make_params())
pars['peak4_center'].set(8664, min=8610, max=8700)

lorentz5 = LorentzianModel(prefix= 'peak5_')
pars.update(lorentz5.make_params())
pars['peak5_center'].set(9322, min=9300, max=9380)

lorentz6 = LorentzianModel(prefix= 'peak6_')
pars.update(lorentz6.make_params())
pars['peak6_center'].set(9936, min=9900, max=10036)


mod= lorentz1 + lorentz2 + lorentz3 + lorentz4 + lorentz5 + lorentz6
init = mod.eval(pars, x=x)
out = mod.fit(ysmooth, pars, x=x)
print(out.fit_report(min_correl=0.5))
plt.plot(df['channel'], ysmooth, 'b')
plt.plot(df['channel'], init, 'r-')
plt.plot(df['channel'], out.best_fit, 'g-')
plt.show()
