Our code takes the data from the Final_Analysis.xlsx spreadsheet stored in the p3-projectpanda repository. There are 3 different
python files, one for each tab in the Final_Analysis spreadsheet. Each one corresponds to an element, Fe2O3, Fe3O4, and Iron Foil.

To run the code in each file all you need to do is type the following commands: 

python Fe2O3Lorentz.py
python Fe3O4Lorentz.py
python IronFoilLorentz.py

Two graphs will be generated. The first will be a graph of the raw data with the edges trimmed and the quadratic effect subtracted, this biases the data. The code also smooths the data then locates and marks the Mossbauer fluorescence peaks.

The second graph includes the smooth, untrimmed data then uses the Lorenzian function to find the peaks.

In addition to the graphs, the code prints the peak data.

PLEASE NOTE: For complete functionality of code, run on a terminal with lmfit or use 'pip install lmfit' and then run with this line unsuppressed. In other words, this code must be run using a terminal that can implement 'from lmfit.models import LinearModel, LorentzianModel' command. If you can't get it to run, try suppressing this line of code in each of the *Lorentz.py files to display graphs with peaks, trendlines, and peak data values.

The mossbauer.py file models data for IronFoilCalc as practice and fits with a lorentzian for Mossbauer fluorescence peaks.
